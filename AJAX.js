function submitForm() {
  const nama = document.getElementById("nama").value;
  const jenisKelamin = document.querySelector('input[name="jenis_kelamin"]:checked').value;
  const hobi = document.getElementById("hobi").value;

  const formData = new FormData();
  formData.append('nama', nama);
  formData.append('jenis_kelamin', jenisKelamin);
  formData.append('hobi', hobi);

  axios.post('process.php', formData)
      .then(response => {
          alert(response.data);
      })
      .catch(error => {
          console.error(error);
      });
}